# Copyright (c) 2021, DFN-CERT Services GmbH
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
"""
Split DFN-CERT AW mail XML attachments by IP address.

This script can be used to split the XML attachments of emails sent as part of
DFN-CERT's AW service into multiple files that each contain only the events
concerning one IP address. The output files are named "<IP address>.xml".
"""
import argparse
import sys
import xml.etree.ElementTree as ET  # nosec - parsing is done with defusedxml
from collections import defaultdict
from pathlib import Path
from typing import Callable, Dict

from defusedxml.ElementTree import parse


def split_mail(xml: argparse.FileType, out: Path):
    """Split XML file xml into multiple files."""
    tree = parse(xml)
    root = tree.getroot()
    iptrees: Dict[str, ET.Element] = defaultdict(root_factory(root))
    for message in root:
        iptrees[message.get("ip")].append(message)
    for ip, iproot in iptrees.items():
        iptree = ET.ElementTree(iproot)
        iptree.write(out / f"{ip}.xml", encoding="utf-8", xml_declaration=True)


def root_factory(root: ET.Element) -> Callable[[], ET.Element]:
    """Return a factory for new tree roots similar to root."""

    def factory():
        return ET.Element(root.tag, root.attrib)

    return factory


def main() -> None:
    """Main functionality when executed as script."""
    args = parse_args()
    split_mail(args.xml, args.out)


def parse_args() -> argparse.Namespace:
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "-x",
        "--xml",
        help="XML attachment file to parse; if ommitted parse stdin",
        type=argparse.FileType("r"),
        default=sys.stdin,
    ),
    parser.add_argument(
        "-o",
        "--out",
        help="directory used for written files; defaults to current directory",
        type=Path,
        default="",
    )
    args = parser.parse_args()
    if not args.out.is_dir():
        parser.error(
            "argument -o/--out: "
            f"'{args.out}' does not exist or is not a directory"
        )
    return args


if __name__ == "__main__":
    main()
