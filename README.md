# dfncert.splitawxml

This package can be used to split the XML attachments of emails sent as part of
DFN-CERT's automatic warning (AW) service into multiple files that each contain
only the events concerning one IP address. The output files are named `<IP
address>.xml`.

Splitting can be performed via split-aw-xml (or the module
dfncert.splitawxml.splitawxml):

```bash
# ls -1
aw.xml
# split-aw-xml -x aw.xml
# ls -1
192.0.2.1.xml
192.0.2.15.xml
aw.xml
```
